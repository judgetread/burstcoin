/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package judgetread.burstcoin;

import judgetread.burstcoin.commands.MainCommand;
import judgetread.burstcoin.listeners.BlockListener;
import judgetread.burstcoin.config.Config;
import judgetread.burstcoin.listeners.InventoryListener;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author judgetread
 */
public class BurstCoin extends JavaPlugin {
    
    private static BurstCoin plugin;
    private final String MIN_JAVA_VERSION = "1.8.0";
    private final long startTime = System.nanoTime();
    
    /**
     * Plugin entry
     */
    @Override
    public void onEnable() {
        Validate.isTrue(MIN_JAVA_VERSION.compareTo(System.getProperty("java.version")) <= 0, 
                "Invalid Java Version Installed: "  + System.getProperty("java.version") + " Minimum Java Version Required: " + MIN_JAVA_VERSION);
        
        /* Set Plugin Instance */
        plugin = this;
        
        /* Display Intro */
        intro();
        
        /* Generate config.yml */
        Config.getInstance();
        
        /* Generate config.yml */
        //BlockManager.getInstance();
        
        /* Register Commands */
        registerCommands();

        /* Inventory Listener */
        registerListeners();
        
        /* Loadtime */
        loadTime();

        /* Outro */
        outro();
        
    }

    /**
     * Return this plugins instance
     *
     * @return BurstCoin
     */
    public static BurstCoin getInstance() {
        return plugin;
    }

    /**
     * Print message to console<BR>
     * Support &color codes.
     *
     * @param message
     */
    public void printConsole(String message) {
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

    /**
     * Print intro message to console.
     */
    private void intro(){
        this.printConsole("&3    ____                  __             _     ");
        this.printConsole("&3   / __ )__  ____________/ /_&b_________  (_)___ ");
        this.printConsole("&3  / __  / / / / ___/ ___/ __&b/ ___/ __ \\/ / __ \\");
        this.printConsole("&3 / /_/ / /_/ / /  (__  ) /_&b/ /__/ /_/ / / / / /");
        this.printConsole("&3/_____/\\__,_/_/  /____/\\__/&b\\___/\\____/_/_/ /_/ ");
        this.printConsole("                                  &bVersion: " + this.getDescription().getVersion());
        outro();
    }
    
    
    private void registerCommands(){
        PluginCommand command = this.getCommand("burstcoin");
        command.setExecutor(new MainCommand());
        command.setDescription("burstcoin");
        command.setPermissionMessage(ChatColor.translateAlternateColorCodes('&', "&8>" + "&3" +  this.getDescription().getName() + "&cYou dont have permission to do that!"));
        command.setUsage("&8>&3" + this.getDescription().getName() + "/burstcoin <action> ");

    }
    
    /**
     * Register listeners with the server.
     */
    private void registerListeners(){
        //Bukkit.getPluginManager().registerEvents(new BlockListener(), this);
        Bukkit.getPluginManager().registerEvents(new InventoryListener(), this);
    }
    
    /**
     * Print how load plugin took to load to console..
     */
    private void loadTime(){
        this.printConsole("&8>&3LOADTIME: &7" + (System.nanoTime() - startTime) / 1000000 + "ms");
    }
    
    /**
     * Print outro to console
     */
    private void outro(){
        this.printConsole("&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=&7=&8=");
    }
    
}
