/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package judgetread.burstcoin.commands;

import judgetread.burstcoin.BurstCoin;
import judgetread.burstcoin.commands.subCommands.Show;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 *
 * @author o0_pa
 */
public class MainCommand   implements CommandExecutor {

    private static final BurstCoin PLUGIN = BurstCoin.getInstance();
    private ISubCommand subCommand;

    /**
     * Constructor
     */
    public MainCommand() {
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        
        if( args.length <= 0){
            this.subCommand = (ISubCommand) new Show(sender, args);
        }
        
        if (args.length > 0) {
            switch (args[0].toUpperCase()) {
                case "SHOW":
                    this.subCommand = (ISubCommand) new Show(sender, args);
                    break;
                case "TWO":
                    this.subCommand = (ISubCommand) new Show(sender, args);
                    break;
                default:
                    return false;
            }
        }
        return this.subCommand != null ? this.subCommand.execute() : false;
    }
    
}