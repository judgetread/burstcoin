/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package judgetread.burstcoin.commands.subCommands;

import judgetread.burstcoin.commands.ISubCommand;
import judgetread.burstcoin.inventory.BCInventory;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author o0_pa
 */
public class Show extends SubCommand implements ISubCommand {

    

    /**
     * Constructor
     *
     * @param sender
     * @param args
     */
    public Show(CommandSender sender, String[] args) {
        super(sender, args);
    }
    
    
    @Override
    public boolean execute() {
        Player player = (Player)sender;
        player.openInventory(new BCInventory(player).getInventory());
        return true;
    }
    
}
