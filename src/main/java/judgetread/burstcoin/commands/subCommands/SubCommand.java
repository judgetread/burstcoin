/*
 * The MIT License
 *
 * Copyright 2018 judgetread.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package judgetread.burstcoin.commands.subCommands;

import judgetread.burstcoin.BurstCoin;
import judgetread.burstcoin.commands.ISubCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 *
 * @author judgetread
 */
abstract class SubCommand implements ISubCommand {

    protected static final BurstCoin PLUGIN = BurstCoin.getInstance();
    protected final CommandSender sender;
    protected final String[] args;
    

    public SubCommand(CommandSender sender, String[] args) {
        this.sender = sender;
        this.args = args;
    }
    
    
    protected boolean consoleOnly(){
            PLUGIN.printConsole("&8>" + "&3" +  PLUGIN.getDescription().getName() + ": &7Cannot run this command from console.");
            return true;
    }

}
