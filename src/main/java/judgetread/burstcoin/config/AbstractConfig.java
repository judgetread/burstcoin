package judgetread.burstcoin.config;

import com.sun.security.auth.login.ConfigFile;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import judgetread.burstcoin.BurstCoin;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;

/**
 * @formatter:off
 */
public class AbstractConfig extends AutoUpdateConfigLoader {

    private static final BurstCoin PLUGIN = BurstCoin.getInstance();

    public AbstractConfig(String fileName) {
        super(fileName, false, false);
    }


    @Override
    protected boolean validateKeys() {
        List<String> reason = new ArrayList<String>();

        return configErrors(reason);
    }

    @Override
    protected void loadKeys() {
    }

    
    public void load() {
        super.loadFile();
    }
    
    public void addSection(String str, ConfigurationSection cs){
            config.set(str, cs);
            save();
    }
}
