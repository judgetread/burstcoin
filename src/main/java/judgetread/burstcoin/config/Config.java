package judgetread.burstcoin.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;

/**
 * Loads config.yml
 */
public class Config extends AutoUpdateConfigLoader {

    private static Config instance;

    /* Private Constructor */
    private Config() {
        super("config.yml");
        validate();
    }

    /**
     * Get the singleton instance of this class.
     *
     * @return Config
     */
    public static Config getInstance() {
        if (instance == null) {
            instance = new Config();
        }
        return instance;
    }

    /**
     * Run validation checks on configuration values.<BR>
     * Add any errors to the reason arraylist. The plguin will then display all
     * error in the arraylist to the console.
     *
     * @return boolean
     */
    @Override
    protected boolean validateKeys() {
        List<String> reason = new ArrayList<>();


        return configErrors(reason);
    }

    /**
     * Get the servers wallet address from config.yml
     *
     * @return String
     */
    public String getServerAddressRS() {
        return config.getString("server.addressRS");
    }

    /**
     * Get the URL of the wallet the server is using.
     *
     * @return String
     */
    public String getServerAPI() {
        return config.getString("server.api.url");
    }
    
    public String getViewTitle(String sec) {
        return config.getString("view." + sec + ".title", "BurstCoin MC Wallet");
    }
    
    public String getViewBase64String(String sec, String loc) {
        return config.getString("view." + sec + ".icons." + loc + ".base64String", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYjVhZmJjYzc0ZjRmMzk5YTJlYWJlNWY1ZjBkNDc2MjMwYmMwZDU0MGZlNWY3ZjMxOTViNGJhNWY2MGVmOTlhOSJ9fX0====");
    }
    public String getViewTitle(String sec, String loc) {
        return config.getString("view." + sec + ".icons." + loc + ".title", "Config Title Missing");
    }
    public int getViewPosition(String sec, String loc) {
        return config.getInt("view." + sec + ".icons." + loc + ".position", 0);
    }
    public int getViewStackSize(String sec, String loc) {
        return config.getInt("view." + sec + ".icons." + loc + ".stack.size", 1);
    }
    public List<String> getViewLore(String sec, String loc) {
        return config.getStringList("view." + sec + ".icons." + loc + ".lore");
    }
    
    public ConfigurationSection getViewIcons(String sec) {
        return config.getConfigurationSection("view." + sec + ".icons");
    }
    
    
    
    
    
    
    //////////////////////
    
    public ConfigurationSection getViewSetsIcons(String sec) {
        return config.getConfigurationSection("view." + sec + ".sets");
    }
    
    public List<String> getSetList(String sec, String loc){
        return config.getStringList("view." + sec + ".sets." + loc);
        
    }
    
    public List<String> getSetListBase64Strings(String sec, String loc){
        return config.getStringList("view." + sec + ".sets." + loc + ".base64Strings");
        
    }
    
    public String getSetTitle(String sec, String loc){
        return config.getString("view." + sec + ".sets." + loc + ".set.title");
    }
    
    public int getSetPosition(String sec, String loc){
        return config.getInt("view." + sec + ".sets." + loc + ".set.position");
    }
    
    public List<String> getSetLore(String sec, String loc){
        return config.getStringList("view." + sec + ".sets." + loc + ".set.lore");
    }
    
    public String getSetIconTitle(String sec, String loc){
        return config.getString("view." + sec + ".sets." + loc + ".icon.title");
    }
    
    public List<String> getSetIconLore(String sec, String loc){
        return config.getStringList("view." + sec + ".sets." + loc + ".icon.lore");
    }
    
    public String getGenerateLoginTitle(){ return config.getString("view.doLogin.login.title");}
    public String getGenerateLoginBase64(){ return config.getString("view.doLogin.login.base64String");}
    public int getGenerateLoginPosition(){ return config.getInt("view.doLogin.login.position");}
    public List<String> getGenerateLoginLore(){ return config.getStringList("view.doLogin.login.lore");}
    
    
    /* Confirmaed Transaction Template */
    public String getTransactionTitle(){ return config.getString("view.transactions.template.title");}
    public String getTransactionBase64(){ return config.getString("view.transactions.template.base64String");}
    public int getTransactionPosition(){ return config.getInt("view.transactions.template.position");}
    public List<String> getTransactionLore(){ return config.getStringList("view.transactions.template.lore");}
    
    /* Main Menu Icon */
    public String getMainMenuIconTitle(){ return config.getString("view.main.mainmenu.title");}
    public String getMainMenuIconBase64(){ return config.getString("view.main.mainmenu.base64String");}
    public int getMainMenuIconPosition(){ return config.getInt("view.main.mainmenu.position");}
    public List<String> getMainMenuIconLore(){ return config.getStringList("view.main.mainmenu.lore");}
    
    /* Loading Title Animation */
    public int getLoadingDelay(){ return config.getInt("view.logingblockchain.delay");}
    public List<String> getLoadingList(){ return config.getStringList("view.logingblockchain.title");}
    
    
    
    
    
    
    
    
    /**
     *
     */
    @Override
    protected void loadKeys() {
    }

}
