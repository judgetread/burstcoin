package judgetread.burstcoin.config;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import judgetread.burstcoin.BurstCoin;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.configuration.ConfigurationSection;

public abstract class ConfigLoader {

    private static final BurstCoin PLUGIN = BurstCoin.getInstance();
    protected String fileName;
    protected File configFile;
    protected FileConfiguration config;
    protected boolean checkKeys = true;

    public ConfigLoader(String relativePath, String fileName) {
        this.fileName = fileName;
        configFile = new File(PLUGIN.getDataFolder(), relativePath + File.separator + fileName);
        loadFile();
    }

    public ConfigLoader(String fileName) {
        this.fileName = fileName;
        configFile = new File(PLUGIN.getDataFolder(), fileName);
        loadFile();
    }

    public ConfigLoader(String fileName, boolean checkKeys) {
        this.fileName = fileName;
        this.checkKeys = checkKeys;
        configFile = new File(PLUGIN.getDataFolder(), fileName);
        loadFile();
    }

    public ConfigLoader(String fileName, boolean checkKeys, boolean validate) {
        this.fileName = fileName;
        this.checkKeys = checkKeys;
        configFile = new File(PLUGIN.getDataFolder(), fileName);
        loadFile();
    }

    protected void loadFile(boolean checkKeys) {
        this.checkKeys = checkKeys;
        loadFile();
    }

    public void set(String a, ConfigurationSection b) {
        config.set(a, b);
    }

    public void save() {
        try {
            config.save(configFile);
        } catch (IOException ex) {
            Logger.getLogger(ConfigLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void loadFile() {
        if (!configFile.exists()) {

            if (this.checkKeys) {
                PLUGIN.printConsole("&8>&3CREATING:&7 " + fileName);
            }
            try {
                PLUGIN.saveResource(fileName, false); // Normal files
            } catch (IllegalArgumentException ex) {
                PLUGIN.saveResource(configFile.getParentFile().getName() + File.separator + fileName, false); // Mod files
            }
        } else {
            if (this.checkKeys) {
                PLUGIN.printConsole("&8>&3LOADING:&7 " + fileName);
            }
        }

        config = YamlConfiguration.loadConfiguration(configFile);
    }

    protected abstract void loadKeys();

    protected boolean validateKeys() {
        return true;
    }

    protected boolean configErrors(List<String> issues) {
        issues.forEach((issue) -> {
            Bukkit.getConsoleSender().sendMessage(issue);
        });

        return issues.isEmpty();
    }

    protected void validate() {
        if (validateKeys()) {
            if (checkKeys) {
                PLUGIN.printConsole("&8>&3CHECKING:&7 No errors found in " + fileName + "!");
            }
        } else {
            Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Errors were found in " + fileName + "! BurstPay can not continue!");
            PLUGIN.getServer().getPluginManager().disablePlugin(PLUGIN);
        }
    }

    public void backup() {
        PLUGIN.getLogger().warning("&8>&3INFO:&3 You are using an old version of the " + fileName + " file.");
        PLUGIN.getLogger().warning("&8>&3INFO:&3 Your old file has been renamed to " + fileName + ".old and has been replaced by an updated version.");

        configFile.renameTo(new File(configFile.getPath() + ".old"));

        if (PLUGIN.getResource(fileName) != null) {
            PLUGIN.saveResource(fileName, true);
        }

        PLUGIN.getLogger().warning("&8>&3INFO:&3 Reloading " + fileName + " with new values...");
        loadFile();
        loadKeys();
    }

}
