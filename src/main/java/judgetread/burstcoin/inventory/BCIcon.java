/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package judgetread.burstcoin.inventory;

import java.util.List;
import java.util.Map;
import judgetread.burstcoin.utils.SkullCreator;
import judgetread.burstcoin.utils.StrUtils;
import org.apache.commons.lang.Validate;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author judgetread
 */
public class BCIcon implements Cloneable{

    private String title;
    private int stackSize;
    private int position;
    private List<String> lore;
    private boolean glowing;
    private ItemStack itemStack;
    private ItemMeta itemMeta;
    private String base64String;
    private String name;
    private int arrayIndex = 0;

    public BCIcon(String name, String base64String, String title, int position, int stackSize, List<String> lore) {
        Validate.notNull(title);
        Validate.isTrue(stackSize > 0);
        Validate.isTrue(position >= 0);
        Validate.notNull(lore);
        this.name = name;
        this.base64String = base64String;
        this.title = StrUtils.convertText(title);
        this.position = position;
        this.stackSize = stackSize;
        this.lore = StrUtils.convertText(lore);
        this.itemStack = buildIcon();
    }

    private ItemStack buildIcon() {
        itemStack = SkullCreator.itemFromBase64(getBase64String());
        itemMeta = getItemStack().getItemMeta();
        itemMeta.setDisplayName(title);
        itemMeta.setLore(lore);
        itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        itemMeta.addItemFlags(ItemFlag.HIDE_DESTROYS);
        itemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        itemMeta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        itemMeta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        itemStack.setAmount(stackSize);
        itemStack.setItemMeta(itemMeta);
        return getItemStack();
    }

    public ItemStack getItemStack() {
        return itemStack != null ? itemStack : buildIcon();
    }

    @Override
    public String toString() {
        return "BCIcon{" + "title=" + title + ", stackSize=" + stackSize + ", position=" + getPosition() + ", lore=" + lore + ", glowing=" + glowing + ", itemStack=" + getItemStack() + '}';
    }

    /**
     * @return the position
     */
    public int getPosition() {
        return position;
    }
    public void setPosition(int position) {
        this.position = position;
    }
    
    public String getName(){
        return name;
    }

    @Override
    public BCIcon clone() throws CloneNotSupportedException {

        try {
            BCIcon c = (BCIcon) super.clone();
            return c;
        } catch (CloneNotSupportedException e) {
            // Will not happen in this case
            return null;
        }
    }

    /**
     * @return the arrayIndex
     */
    public int getArrayIndex() {
        return arrayIndex;
    }

    /**
     * @param arrayIndex the arrayIndex to set
     */
    public void setArrayIndex(int arrayIndex) {
        this.arrayIndex = arrayIndex;
    }

    /**
     * @return the base64String
     */
    public String getBase64String() {
        return base64String;
    }
    
}
