/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package judgetread.burstcoin.inventory;

import java.util.ArrayList;
import java.util.List;
import judgetread.burstcoin.BurstCoin;
import judgetread.burstcoin.inventory.views.StartView;
import judgetread.burstcoin.utils.StrUtils;
import judgetread.burstcoin.utils.TitleUpdater;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

/**
 *
 * @author o0_pa
 */
public class BCInventory implements IBCInventory {

    private Inventory inventory;
    private final Player player;
    private BCView view;

    public BCInventory(Player player) {
        this.player = player;
        this.inventory = null;
        view = new StartView();
        createInventory();
        loadView(view);
    }

    /*
    *
     */
    private void createInventory() {
        inventory = Bukkit.createInventory(this, 54, StrUtils.convertText(view.getViewTitle()));
    }

    /**
     *
     * @return
     */
    @Override
    public Inventory getInventory() {
        return inventory;
    }

    /**
     *
     * @param view
     */
    @Override
    public final void loadView(BCView view) {
        this.view = view;

        inventory.clear();

        view.getIcons().forEach((icon) -> {
            this.inventory.setItem(icon.getPosition(), icon.getItemStack());
        });

        TitleUpdater.updateTitle(player, view.getViewTitle());
        player.updateInventory();

    }

    @Override
    public void updateItem(int position, ItemStack itemStack) {
        inventory.setItem(position, itemStack);
        updatePlayerInventory();
    }

    @Override
    public void updatePlayerInventory() {
        player.updateInventory();
    }

    @Override
    public void onInventoryClick(InventoryClickEvent event
    ) {
        event.setCancelled(true);
        if (event.getRawSlot() > event.getInventory().getSize()) {
            return;
        }
        if (event.getSlotType() == InventoryType.SlotType.OUTSIDE) {
            return;
        }

        view.viewClick(event);

    }

    /**
     *
     */
    @Override
    public void updateData() {
        List<String> loadingText = new ArrayList<>();
        loadingText.add("Fetching fresh burst data ..");
        loadingText.add("Fetching fresh burst data ....");
        loadingText.add("Fetching fresh burst data ......");

        BukkitTask task = new BukkitRunnable() {
            int x = 0;

            @Override
            public void run() {

                TitleUpdater.updateTitle(player, loadingText.get(x));
                x++;
                if (x >= loadingText.size()) {
                    x = 0;
                }
            }
        }.runTaskTimer(BurstCoin.getInstance(), 0, 8);

        this.view.update(() -> {
            task.cancel();
            loadView(view);
        });
    }

}
