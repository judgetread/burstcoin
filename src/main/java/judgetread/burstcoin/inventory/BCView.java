/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package judgetread.burstcoin.inventory;

import java.util.List;
import judgetread.burstcoin.BurstCallback;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 *
 * @author o0_pa
 */
public interface BCView {

    /**
     * @return the icons
     */
    List<BCIcon> getIcons();

    /**
     *
     * @return
     */
    String getViewTitle();
    
    /**
     *
     * @param event
     */
    void viewClick(InventoryClickEvent event);

    void update(BurstCallback burstCallback);

    /**
     * @return the accountRS
     */
    String getAccountRS();

    /**
     * @param accountRS the accountRS to set
     */
    void setAccountRS(String accountRS);

}
