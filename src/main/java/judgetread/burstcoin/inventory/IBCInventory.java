/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package judgetread.burstcoin.inventory;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author o0_pa
 */
public interface IBCInventory   extends InventoryHolder {
    
    
    void onInventoryClick(InventoryClickEvent event);

    /**
     *
     * @param view
     */
    void loadView(BCView view);

    void updatePlayerInventory();

    void updateItem(int position, ItemStack itemStack);

    /**
     *
     */
    void updateData();

}
