/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package judgetread.burstcoin.inventory.views;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import judgetread.burstcoin.config.Config;
import judgetread.burstcoin.inventory.BCIcon;
import judgetread.burstcoin.inventory.BCView;
import judgetread.burstcoin.utils.StrUtils;
import org.apache.commons.lang.Validate;

/**
 *
 * @author o0_pa
 */
public abstract class AbstractBCView implements BCView {

    protected final String sec;
    protected String title;
    protected List<BCIcon> viewIcons = new ArrayList<>();
    protected Map<String, String> placeholders;
    protected String accountRS;

    /**
     *
     * @param sec
     */
    public AbstractBCView(String sec) {
        Validate.notNull(sec);
        this.sec = sec;
        this.placeholders = new HashMap<>();
        loadTitleFromConfig();
    }
    
    /**
     *
     */
    protected final void loadTitleFromConfig() {
        title = Config.getInstance().getViewTitle(this.sec);
    }

    /**
     *
     */
    protected final void loadIconsFromConfig() {
        
        Config.getInstance().getViewIcons(sec).getKeys(false).forEach((String loc) -> {
            viewIcons.add(new BCIcon(
                    loc,
                    Config.getInstance().getViewBase64String(sec, loc),
                    StrUtils.applyPH(placeholders, Config.getInstance().getViewTitle(sec, loc)),
                    Config.getInstance().getViewPosition(sec, loc),
                    Config.getInstance().getViewStackSize(sec, loc),
                    StrUtils.applyPH(placeholders, Config.getInstance().getViewLore(sec, loc))
            ));
        });
    }

    /**
     *
     * @return
     */
    @Override
    public String getViewTitle(){
        return this.title;
    }
    
    /**
     * @return the viewIcons
     */
    @Override
    public List<BCIcon> getIcons() {
        return viewIcons;
    }

    protected Optional<BCIcon> isViewSlot(int slot) {
        return viewIcons.stream().filter((BCIcon _icon) -> _icon.getPosition() == slot).findFirst();
    }

    /**
     * @return the accountRS
     */
    public String getAccountRS() {
        return accountRS;
    }

    /**
     * @param accountRS the accountRS to set
     */
    public void setAccountRS(String accountRS) {
        this.accountRS = accountRS;
    }


}
