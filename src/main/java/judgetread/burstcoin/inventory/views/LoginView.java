/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package judgetread.burstcoin.inventory.views;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import judgetread.burstcoin.BurstCallback;
import judgetread.burstcoin.BurstCoin;
import judgetread.burstcoin.config.Config;
import judgetread.burstcoin.inventory.BCIcon;
import judgetread.burstcoin.inventory.IBCInventory;
import judgetread.burstcoin.utils.StrUtils;
import judgetread.burstcoin.utils.TitleUpdater;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

/**
 *
 * @author o0_pa
 */
public class LoginView extends AbstractBCView {

    private final Map<String, List<BCIcon>> iconSets;
    private final List<Integer> passPatternSlots;
    private String currentSet;

    public LoginView() {
        super("doLogin");
        this.passPatternSlots = new ArrayList<>(Arrays.asList(19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34));
        this.iconSets = new LinkedHashMap<>();
        this.currentSet = Config.getInstance().getViewSetsIcons(this.sec).getKeys(false).toArray()[0].toString();
        addLoginIcon();
        loadSets();
        loadSetIcons();
        populatePatternIcons();

    }


    private void addLoginIcon() {
        viewIcons.add(new BCIcon(
                "login",
                Config.getInstance().getGenerateLoginBase64(),
                StrUtils.applyPH(placeholders, Config.getInstance().getGenerateLoginTitle()),
                Config.getInstance().getGenerateLoginPosition(),
                1,
                StrUtils.applyPH(placeholders, Config.getInstance().getGenerateLoginLore())
        ));
    }

    private void populatePatternIcons() {
        passPatternSlots.forEach((i) -> {
            try {
                BCIcon ci = iconSets.get(currentSet).get(0).clone();
                ci.setPosition(i);
                viewIcons.add(ci);
            } catch (CloneNotSupportedException ex) {
                Logger.getLogger(LoginView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    private void loadSetIcons() {
        Config.getInstance().getViewSetsIcons(this.sec).getKeys(false).forEach((loc) -> {
            viewIcons.add(new BCIcon(
                    loc,
                    Config.getInstance().getSetListBase64Strings(this.sec, loc).get(0),
                    StrUtils.applyPH(placeholders, Config.getInstance().getSetTitle(sec, loc)),
                    Config.getInstance().getSetPosition(sec, loc),
                    1,
                    StrUtils.applyPH(placeholders, Config.getInstance().getSetLore(sec, loc))
            ));
        });
    }

    private void loadSets() {
        for (String loc : Config.getInstance().getViewSetsIcons(this.sec).getKeys(false)) {
            List<BCIcon> tmpIconSet = new ArrayList<>();
            for (String setByBase64String : Config.getInstance().getSetListBase64Strings(this.sec, loc)) {
                tmpIconSet.add(new BCIcon(
                        "sss",
                        setByBase64String,
                        StrUtils.applyPH(placeholders, Config.getInstance().getSetIconTitle(sec, loc)),
                        Config.getInstance().getSetPosition(sec, loc),
                        1,
                        StrUtils.applyPH(placeholders, Config.getInstance().getSetIconLore(sec, loc))
                ));
            }
            iconSets.put(loc, tmpIconSet);
        }
    }

    @Override
    public void viewClick(InventoryClickEvent event) {

        Optional<IBCInventory> bcInventory = Optional.ofNullable((IBCInventory) event.getInventory().getHolder());
        Optional<BCIcon> bcIcon = isViewSlot(event.getSlot());

        if (!bcIcon.isPresent() || !bcInventory.isPresent()) {
            return;
        }

        if (passPatternSlots.contains(event.getRawSlot())) {
            for (BCIcon i : viewIcons) {
                if (i.getPosition() == event.getRawSlot()) {

                    if (i.getArrayIndex() >= iconSets.get(currentSet).size() - 1) {
                        i.setArrayIndex(0);
                    } else {
                        i.setArrayIndex(i.getArrayIndex() + 1);
                    }

                    ((IBCInventory) event.getInventory().getHolder()).updateItem(i.getPosition(), iconSets.get(currentSet).get(i.getArrayIndex()).getItemStack());
                    break;
                }
            }
        }
        
        for (BCIcon i : viewIcons) {
            if (i.getPosition() == event.getRawSlot() && iconSets.containsKey(i.getName())) {
                currentSet = i.getName();
                loadSets();
                loadSetIcons();
                populatePatternIcons();
                ((IBCInventory) event.getInventory().getHolder()).loadView(this);
                break;
            } else if (i.getPosition() == event.getRawSlot() && i.getName().equalsIgnoreCase("login")) {
                StringBuilder passPhrase = new StringBuilder();
                int setInt = 3;
                int index = 3;

                for (Map.Entry<String, List<BCIcon>> key : iconSets.entrySet()) {
                    if (key.getKey().equalsIgnoreCase(currentSet)) {
                        setInt = index;
                    }
                    index++;
                }

                for (BCIcon _viewIcon : viewIcons) {
                    if (passPatternSlots.contains(_viewIcon.getPosition())) {
                        passPhrase.append(StrUtils.getWord((_viewIcon.getPosition()) + ((_viewIcon.getArrayIndex() + 2) * (setInt * StrUtils.getFirstInt(event.getInventory().getViewers().get(0).getUniqueId().toString())))));
                        passPhrase.append(" ");
                    }
                }

                MainView mainView = new MainView(passPhrase.toString().trim());

                BukkitTask task = new BukkitRunnable() {
                    int x = 0;

                    @Override
                    public void run() {

                        TitleUpdater.updateTitle((Player) event.getInventory().getHolder().getInventory().getViewers().get(0), Config.getInstance().getLoadingList().get(x));
                        x++;
                        if (x >= Config.getInstance().getLoadingList().size()) {
                            x = 0;
                        }
                    }
                }.runTaskTimer(BurstCoin.getInstance(), 0, Config.getInstance().getLoadingDelay());

                mainView.update(() -> {
                    task.cancel();
                    mainView.loadIcons();
                    ((IBCInventory) event.getInventory().getHolder()).loadView(mainView);
                });
            }

        }

    }

    @Override
    public void update(BurstCallback burstCallback) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
