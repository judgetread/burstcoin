/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package judgetread.burstcoin.inventory.views;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.nio.charset.Charset;
import java.util.Optional;
import judgetread.burstcoin.BurstCallback;
import judgetread.burstcoin.BurstCoin;
import judgetread.burstcoin.inventory.BCIcon;
import judgetread.burstcoin.inventory.IBCInventory;
import judgetread.burstcoin.utils.CryptoUtils;
import judgetread.burstcoin.utils.JsonUtils;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 *
 * @author o0_pa
 */
public class MainView extends AbstractBCView {

    private final String secretKey;
    private byte[] publicKey;
    private Long accountNumerical;
    private String accountName = "";
    private Double unconfirmedBalanceNQT = 0D;
    private Double guaranteedBalanceNQT = 0D;
    private Double effectiveBalanceNXT = 0D;
    private Double forgedBalanceNQT = 0D;
    private Double balanceNQT = 0D;
    
    private String rewardRecipient = "None";
    private int suggestedFee = 01000000;
    
    private int accountTransactions = 0;
    private int accountUnconfirmedTransactions = 0;
    
    private Double sentTotal = 0D;
    private Double receivedTotal = 0D;
    private Double feesTotal = 0D;

    private Optional<JsonArray> transactions = Optional.empty();
    private Optional<JsonArray> transactionTypes = Optional.empty();
    private Optional<JsonArray> unconfirmedTransactions = Optional.empty();
    private Optional<JsonObject> account = Optional.empty();
    private Optional<JsonObject> recipient = Optional.empty();
    private Optional<JsonObject> suggestFees = Optional.empty();

    public MainView(String secretKey) {
        super("main");
        this.secretKey = secretKey;
        this.publicKey = CryptoUtils.getPublicKey(this.secretKey.getBytes(Charset.forName("UTF-8")));
        this.accountNumerical = CryptoUtils.getAddressNum(this.publicKey);
        this.accountRS = CryptoUtils.getRS(accountNumerical);
        this.title = this.accountRS;

    }
    
    public void loadIcons(){
        
            
        placeholders.put("secretKey", this.secretKey);
        placeholders.put("publicKey", String.valueOf(this.publicKey));
        placeholders.put("accountNumerical", String.valueOf(this.accountNumerical));
        placeholders.put("accountRS", accountRS);

        placeholders.put("unconfirmedBalanceNQT", String.format("%1$,.8f", unconfirmedBalanceNQT / 100000000));
        placeholders.put("guaranteedBalanceNQT", String.format("%1$,.8f", guaranteedBalanceNQT / 100000000));
        placeholders.put("effectiveBalanceNXT", String.format("%1$,.8f", effectiveBalanceNXT / 100000000));
        placeholders.put("forgedBalanceNQT", String.format("%1$,.8f", forgedBalanceNQT / 100000000));
        placeholders.put("balanceNQT", String.format("%1$,.8f", balanceNQT / 100000000));
        placeholders.put("accountName", accountName);

        placeholders.put("rewardRecipient", rewardRecipient);
        placeholders.put("suggestedFee", String.valueOf(suggestedFee));
        
        placeholders.put("unconfirmedTransactions", String.valueOf(accountUnconfirmedTransactions));
        placeholders.put("accountTransactions", String.valueOf(accountTransactions));
        
        placeholders.put("sentTotal", String.format("%1$,.8f", sentTotal / 100000000));
        placeholders.put("receivedTotal", String.format("%1$,.8f", receivedTotal / 100000000));
        placeholders.put("feesTotal", String.format("%1$,.8f", feesTotal / 100000000));

        loadIconsFromConfig();
    }

    /**
     *
     * @param burstCallback
     */
    @Override
    public void update(BurstCallback burstCallback) {
        Bukkit.getServer().getScheduler().scheduleAsyncDelayedTask(BurstCoin.getInstance(), () -> {
            transactions = JsonUtils.getInstance().getTransactions(accountRS);
            transactionTypes = JsonUtils.getInstance().getTransactionTypes();
            unconfirmedTransactions = JsonUtils.getInstance().getUnconfirmedTransactions(accountRS);
            account = JsonUtils.getInstance().getAccount(getAccountRS());
            recipient = JsonUtils.getInstance().getRewardRecipient(getAccountRS());
            suggestFees = JsonUtils.getInstance().getSuggestedFee();

            if (account.isPresent()) {
                setUnconfirmedBalanceNQT();
                setGuaranteedBalanceNQT();
                setEffectiveBalanceNXT();
                setForgedBalanceNQT();
                setBalanceNQT();
                setAccountName();
            }
            if (recipient.isPresent()) {
                setRewardRecipient();
            }
            if (suggestFees.isPresent()) {
                setSuggestedFee();
            }
            if (transactions.isPresent()) {
                setAccountTransactions();
                setTransactionsTotals();
            }
            if (unconfirmedTransactions.isPresent()) {
                setAccountUnconfirmedTransactions();
            }

            burstCallback.done();
        });
    }

    @Override
    public void viewClick(InventoryClickEvent event) {
        Validate.notNull(event);
        Validate.isTrue(event.getInventory().getHolder() instanceof IBCInventory);

        Optional<IBCInventory> bcInventory = Optional.ofNullable((IBCInventory) event.getInventory().getHolder());
        Optional<BCIcon> bcIcon = isViewSlot(event.getSlot());

        if (!bcIcon.isPresent() || !bcInventory.isPresent()) {
            return;
        }

        switch (bcIcon.get().getName().toUpperCase()) {
            case "SHOP":
                bcInventory.get().loadView(new ShopView(this));
                break;
            case "TRANSACTIONS":
                bcInventory.get().loadView(new TransactionsView(accountRS, transactions, transactionTypes, this));
                break;
            case "LOGOUT":
                bcInventory.get().loadView(new LoginView());
                break;
            default:
                break;
        }

    }

    /* UnconfirmedBalanceNQT */
    private Double getUnconfirmedBalanceNQT() {
        if (account.get().get("unconfirmedBalanceNQT") != null) {
            return account.get().get("unconfirmedBalanceNQT").getAsDouble();
        }
        return 0D;
    }

    private void setUnconfirmedBalanceNQT() {
        unconfirmedBalanceNQT = getUnconfirmedBalanceNQT();
    }

    /* GuaranteedBalanceNQT */
    private Double getGuaranteedBalanceNQT() {
        if (account.get().get("guaranteedBalanceNQT") != null) {
            return account.get().get("guaranteedBalanceNQT").getAsDouble();
        }
        return 0D;
    }

    private void setGuaranteedBalanceNQT() {
        guaranteedBalanceNQT = getGuaranteedBalanceNQT();
    }

    /* effectiveBalanceNXT */
    private Double getEffectiveBalanceNXT() {
        if (account.get().get("effectiveBalanceNXT") != null) {
            return account.get().get("effectiveBalanceNXT").getAsDouble();
        }
        return 0D;
    }

    private void setEffectiveBalanceNXT() {
        effectiveBalanceNXT = getEffectiveBalanceNXT();
    }

    /* forgedBalanceNQT */
    private Double getForgedBalanceNQT() {
        if (account.get().get("forgedBalanceNQT") != null) {
            return account.get().get("forgedBalanceNQT").getAsDouble();
        }
        return 0D;
    }

    private void setForgedBalanceNQT() {
        forgedBalanceNQT = getForgedBalanceNQT();
    }

    /* balanceNQT */
    private Double getBalanceNQT() {
        if (account.get().get("balanceNQT") != null) {
            return account.get().get("balanceNQT").getAsDouble();
        }
        return 0D;
    }

    private void setBalanceNQT() {
        balanceNQT = getBalanceNQT();
    }

    /* name */
    private String getAccountName() {
            Bukkit.getConsoleSender().sendMessage("outside null check");
        if (account.get().get("name") != null) {
            Bukkit.getConsoleSender().sendMessage(account.get().get("name").getAsString());
            return account.get().get("name").getAsString();
        }
        return "";
    }

    private void setAccountName() {
        accountName = getAccountName();
    }

    /* name */
    private String getRewardRecipient() {
        if (recipient.get().get("recipient") != null) {
            return recipient.get().get("recipient").getAsString();
        }
        return "";
    }

    private void setRewardRecipient() {
        rewardRecipient = getRewardRecipient();
    }

    /* setAccountTransactions */
    private int getAccountTransactions() {
        return transactions.get().size();
    }

    private void setAccountTransactions() {
        accountTransactions = getAccountTransactions();
    }

    /* setAccountUnconfirmedTransactions */
    private int getAccountUnconfirmedTransactions() {
        return unconfirmedTransactions.get().size();
    }

    private void setAccountUnconfirmedTransactions() {
        accountUnconfirmedTransactions = getAccountUnconfirmedTransactions();
    }

    /* sentTransactions */
    private int setTransactionsTotals() {
        int sent = 0;
        for (JsonElement s : transactions.get()) {
            if (s.getAsJsonObject().get("amountNQT") != null) {

                if (s.getAsJsonObject().get("senderRS") != null) {
                    // Is sender
                    if (s.getAsJsonObject().get("senderRS").getAsString().equalsIgnoreCase(accountRS)) {
                        sentTotal = sentTotal + s.getAsJsonObject().get("amountNQT").getAsDouble();
                    }

                    // Is sender and paid fees
                    if (s.getAsJsonObject().get("feeNQT") != null) {
                            feesTotal = feesTotal + s.getAsJsonObject().get("feeNQT").getAsDouble();
                    }
                }
                
                // Is recipient
                if (s.getAsJsonObject().get("recipientRS") != null) {
                    if (s.getAsJsonObject().get("recipientRS").getAsString().equalsIgnoreCase(accountRS)) {
                        receivedTotal = receivedTotal + s.getAsJsonObject().get("amountNQT").getAsDouble();
                    }
                }
            }
        }
        return sent;
    }
    
    
    
    private int getSuggestedFee() {
        if (recipient.get().get("standard") != null) {
            return recipient.get().get("standard").getAsInt();
        }
        return 01000000;
    }
    
    private void setSuggestedFee(){
        suggestedFee = getSuggestedFee();
    }

}
