/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package judgetread.burstcoin.inventory.views;

import java.util.Optional;
import judgetread.burstcoin.BurstCallback;
import judgetread.burstcoin.config.Config;
import judgetread.burstcoin.inventory.BCIcon;
import judgetread.burstcoin.inventory.BCView;
import judgetread.burstcoin.inventory.IBCInventory;
import judgetread.burstcoin.utils.StrUtils;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 *
 * @author o0_pa
 */
public class ShopView extends AbstractBCView {

    private BCView lastView;
    
    public ShopView(BCView lastView) {
        super("shop");
        this.lastView = lastView;
        
        // Main Menu
        viewIcons.add(new BCIcon(
                "mainmenu",
                StrUtils.applyPH(placeholders, Config.getInstance().getMainMenuIconBase64()),
                StrUtils.applyPH(placeholders, Config.getInstance().getMainMenuIconTitle()),
                Config.getInstance().getMainMenuIconPosition(),
                1,
                StrUtils.applyPH(placeholders, Config.getInstance().getMainMenuIconLore())
        ));
       // loadIconsFromConfig();
        
        
    }

    @Override
    public void viewClick(InventoryClickEvent event) {
        Validate.notNull(event);
        Validate.isTrue(event.getInventory().getHolder() instanceof IBCInventory);

        Optional<IBCInventory> bcInventory = Optional.ofNullable((IBCInventory) event.getInventory().getHolder());
        Optional<BCIcon> bcIcon = isViewSlot(event.getSlot());

        if (!bcIcon.isPresent() || !bcInventory.isPresent()) {
            return;
        }

        switch (bcIcon.get().getName().toUpperCase()) {
            case "MAINMENU":
                bcInventory.get().loadView(lastView);
            default:
                break;
        }
        

    }

    @Override
    public void update(BurstCallback burstCallback) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
