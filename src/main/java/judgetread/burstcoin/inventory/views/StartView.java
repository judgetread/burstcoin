/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package judgetread.burstcoin.inventory.views;

import java.util.Optional;
import judgetread.burstcoin.BurstCallback;
import judgetread.burstcoin.inventory.BCIcon;
import judgetread.burstcoin.inventory.IBCInventory;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 *
 * @author o0_pa
 */
public class StartView extends AbstractBCView {

    public StartView() {
        super("start");
        loadIconsFromConfig();
    }

    @Override
    public void viewClick(InventoryClickEvent event) {
        Validate.notNull(event);
        Validate.isTrue(event.getInventory().getHolder() instanceof IBCInventory);

        Optional<IBCInventory> bcInventory = Optional.ofNullable((IBCInventory) event.getInventory().getHolder());
        Optional<BCIcon> bcIcon = isViewSlot(event.getSlot());

        if (!bcIcon.isPresent() || !bcInventory.isPresent()) {
            return;
        }
        
        switch (bcIcon.get().getName().toUpperCase()) {
            case "PASSPATTERNICON":
                bcInventory.get().loadView(new LoginView());
                break;
            case "WEB":
                bcInventory.get().getInventory().getViewers().get(0).sendMessage("https://www.burst-coin.org/");
                break;
            case "TWITTER":
                bcInventory.get().getInventory().getViewers().get(0).sendMessage("https://twitter.com/hashtag/burstcoin?lang=en");
                break;
            case "REDDIT":
                bcInventory.get().getInventory().getViewers().get(0).sendMessage("https://www.reddit.com/r/burstcoin/");
                break;
            case "YOUTUBE":
                bcInventory.get().getInventory().getViewers().get(0).sendMessage("https://www.youtube.com/watch?v=Fyj9RIyxLb4&vl=en");
                break;
            case "DISCORD":
                bcInventory.get().getInventory().getViewers().get(0).sendMessage("https://discordapp.com/channels/234305723285110786/234305723285110786");
                break;
            default:
                break;
        }

    }

    @Override
    public void update(BurstCallback burstCallback) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
