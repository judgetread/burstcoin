/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package judgetread.burstcoin.inventory.views;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import java.util.Optional;
import judgetread.burstcoin.BurstCallback;
import judgetread.burstcoin.config.Config;
import judgetread.burstcoin.inventory.BCIcon;
import judgetread.burstcoin.inventory.BCView;
import judgetread.burstcoin.inventory.IBCInventory;
import judgetread.burstcoin.utils.StrUtils;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 *
 * @author o0_pa
 */
public class TransactionsView extends AbstractBCView {

    private Optional<JsonArray> transactions = Optional.empty();
    private Optional<JsonArray> transactionTypes = Optional.empty();
    private BCView lastView;

    public TransactionsView(String accountRS, Optional<JsonArray> transactions, Optional<JsonArray> transactionTypes, BCView lastView) {
        super("transactions");
        this.accountRS = accountRS;
        this.lastView = lastView;
        this.transactions = transactions;
        this.transactionTypes = transactionTypes;
        loadTransactionIcons();

        // Main Menu
        viewIcons.add(new BCIcon(
                "mainmenu",
                StrUtils.applyPH(placeholders, Config.getInstance().getMainMenuIconBase64()),
                StrUtils.applyPH(placeholders, Config.getInstance().getMainMenuIconTitle()),
                Config.getInstance().getMainMenuIconPosition(),
                1,
                StrUtils.applyPH(placeholders, Config.getInstance().getMainMenuIconLore())
        ));
        //loadIconsFromConfig();
    }

    private void loadTransactionIcons() {

        int pos = 0;
        placeholders.clear();

        if (this.transactions.isPresent()) {

            for (JsonElement s : transactions.get()) {

                String type = "";
                String transactionType = "";
                String subtype = "";
                String transactionSubtype = "";
                String transactionID = "";
                String senderRS = "";
                String recipientRS = "";
                Double amountNQT = 0D;
                int confirmations = 0;
                Double feeNQT = 0D;
                String block = "";
                String blockTimestamp = "";

                if (s.getAsJsonObject().get("type") != null) {
                    type = s.getAsJsonObject().get("type").getAsString();

                    for (JsonElement ss : transactionTypes.get()) {
                        if (ss.getAsJsonObject().get("value").getAsString().equalsIgnoreCase(type)) {
                            if (ss.getAsJsonObject().get("description") != null) {
                                transactionType = ss.getAsJsonObject().get("description").getAsString();
                            }
                        }
                    }

                }

                if (s.getAsJsonObject().get("transaction") != null) {
                    transactionID = s.getAsJsonObject().get("transaction").getAsString();
                }

                if (s.getAsJsonObject().get("senderRS") != null) {
                    senderRS = s.getAsJsonObject().get("senderRS").getAsString();
                }

                if (s.getAsJsonObject().get("recipientRS") != null) {
                    recipientRS = s.getAsJsonObject().get("recipientRS").getAsString();
                }

                if (s.getAsJsonObject().get("amountNQT") != null) {
                    amountNQT = s.getAsJsonObject().get("amountNQT").getAsDouble();
                }

                if (s.getAsJsonObject().get("confirmations") != null) {
                    confirmations = s.getAsJsonObject().get("confirmations").getAsInt();
                }

                if (s.getAsJsonObject().get("feeNQT") != null) {
                    feeNQT = s.getAsJsonObject().get("feeNQT").getAsDouble();
                }

                if (s.getAsJsonObject().get("block") != null) {
                    block = s.getAsJsonObject().get("block").getAsString();
                }

                if (s.getAsJsonObject().get("blockTimestamp") != null) {
                    blockTimestamp = s.getAsJsonObject().get("blockTimestamp").getAsString();
                }

                placeholders.put("type", type);
                placeholders.put("transactionType", transactionType);
                placeholders.put("subtype", subtype);
                placeholders.put("transactionSubtype", transactionSubtype);
                placeholders.put("senderRS", senderRS);
                placeholders.put("transactionID", transactionID);
                placeholders.put("recipientRS", recipientRS);
                placeholders.put("amountNQT", String.format("%1$,.8f", amountNQT / 100000000));
                placeholders.put("confirmations", String.valueOf(confirmations));
                placeholders.put("feeNQT", String.format("%1$,.8f", feeNQT / 100000000));
                placeholders.put("block", block);
                placeholders.put("blockTimestamp", blockTimestamp);

                viewIcons.add(new BCIcon(
                        "transactionID",
                        StrUtils.applyPH(placeholders, Config.getInstance().getTransactionBase64()),
                        StrUtils.applyPH(placeholders, Config.getInstance().getTransactionTitle()),
                        pos,
                        1,
                        StrUtils.applyPH(placeholders, Config.getInstance().getTransactionLore())
                ));
                pos++;
            }
        }
    }

    @Override
    public void viewClick(InventoryClickEvent event) {
        Validate.notNull(event);
        Validate.isTrue(event.getInventory().getHolder() instanceof IBCInventory);

        Optional<IBCInventory> bcInventory = Optional.ofNullable((IBCInventory) event.getInventory().getHolder());
        Optional<BCIcon> bcIcon = isViewSlot(event.getSlot());

        if (!bcIcon.isPresent() || !bcInventory.isPresent()) {
            return;
        }

        switch (bcIcon.get().getName().toUpperCase()) {
            case "MAINMENU":
                bcInventory.get().loadView(lastView);
            default:
                break;
        }

    }

    @Override
    public void update(BurstCallback burstCallback) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
