/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package judgetread.burstcoin.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import judgetread.mcburstpay.crypto.Curve25519;
import judgetread.mcburstpay.crypto.ReedSolomon;

/**
 *
 * @author o0_pa
 */
public class CryptoUtils {
    
    
    
    public static String getAddressRS(byte[] secretPhraseBytes) {
        byte[] publicKeyHash = getMessageDigest().digest(getPublicKey(secretPhraseBytes));
        Long accountId = fullHashToId(publicKeyHash);
        System.out.println(accountId);
        return "BURST-" + ReedSolomon.encode(nullToZero(accountId));
    }

    public static String getRS(Long accountId) {
        return "BURST-" + ReedSolomon.encode(nullToZero(accountId));
    }

    public static Long getAddressNum(byte[] secretPhraseBytes) {
        byte[] publicKeyHash = getMessageDigest().digest(getPublicKey(secretPhraseBytes));
        return fullHashToId(publicKeyHash);
    }

    /**
     * Null to zero.
     *
     * @param l the l
     * @return the long
     */
    public static long nullToZero(Long l) {
        return l == null ? 0 : l;
    }

    /**
     * Gets message digest.
     *
     * @return the message digest
     */
    public static MessageDigest getMessageDigest() {
        try {
            return MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Get public key.
     *
     * @param secretPhraseBytes the secret phrase bytes
     * @return the byte [ ]
     */
    public static byte[] getPublicKey(byte[] secretPhraseBytes) {
        byte[] publicKey = new byte[32];
        Curve25519.keygen(publicKey, null, getMessageDigest().digest(secretPhraseBytes));
        return publicKey;
    }

    /**
     * Full hash to id.
     *
     * @param hash the hash
     * @return the long
     */
    public static Long fullHashToId(byte[] hash) {
        if (hash == null || hash.length < 8) {
            throw new IllegalArgumentException("Invalid hash: " + Arrays.toString(hash));
        }
        BigInteger bigInteger = new BigInteger(1, new byte[]{hash[7], hash[6], hash[5], hash[4], hash[3], hash[2], hash[1], hash[0]});
        return bigInteger.longValue();
    }
}
