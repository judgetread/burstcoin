/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package judgetread.burstcoin.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.MalformedJsonException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import judgetread.burstcoin.config.Config;
import org.apache.commons.lang.Validate;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author judgetread
 */
public class JsonUtils {

    private static JsonUtils instance;
    private String apiServerURL;

    /*
    * Private Constructor
     */
    private JsonUtils() {
        this.apiServerURL = Config.getInstance().getServerAPI();
    }

    /**
     * Get singleton instance of this class.
     *
     * @return Instance of BurstManager.
     */
    public static JsonUtils getInstance() {
        if (instance == null) {
            instance = new JsonUtils();
        }
        return instance;
    }

    /**
     *
     * @param accountRS
     * @return
     */
    public Optional<JsonObject> getSuggestedFee() {

        Map<String, String> map = new HashMap<>();
        map.put("requestType", "suggestFee");
        return Optional.ofNullable(getJsonObject(getParams(map)));
    }

    /**
     *
     * @param accountRS
     * @return
     */
    public Optional<JsonObject> getRewardRecipient(String accountRS) {
        Validate.notEmpty(accountRS);

        Map<String, String> map = new HashMap<>();
        map.put("requestType", "getRewardRecipient");
        map.put("account", accountRS);
        return Optional.ofNullable(getJsonObject(getParams(map)));
    }

    /**
     *
     * @param accountRS
     * @return
     */
    public Optional<JsonArray> getTransactionTypes() {

        Map<String, String> map = new HashMap<>();
        map.put("requestType", "getConstants");

        Optional<JsonElement> a = Optional.ofNullable(getJsonObject(getParams(map)).get("transactionTypes"));
        if (a.isPresent()) {
            return Optional.ofNullable(getJsonObject(getParams(map)).get("transactionTypes").getAsJsonArray());
        }
        return Optional.empty();
    }

    /**
     *
     * @param accountRS
     * @return
     */
    public Optional<JsonObject> getAccount(String accountRS) {
        Validate.notEmpty(accountRS);

        Map<String, String> map = new HashMap<>();
        map.put("requestType", "getAccount");
        map.put("account", accountRS);
        return Optional.ofNullable(getJsonObject(getParams(map)));
    }

    /**
     *
     * @param accountRS
     * @return
     */
    public Optional<JsonArray> getTransactions(String accountRS) {
        Validate.notEmpty(accountRS);

        Map<String, String> map = new HashMap<>();
        map.put("requestType", "getAccountTransactions");
        map.put("account", accountRS);
        map.put("numberOfConfirmations", "1"); 

        Optional<JsonElement> a = Optional.ofNullable(getJsonObject(getParams(map)).get("transactions"));
        if (a.isPresent()) {
            return Optional.ofNullable(getJsonObject(getParams(map)).get("transactions").getAsJsonArray());
        }
        return Optional.empty();
    }

    /**
     *
     * @param accountRS
     * @return
     */
    public Optional<JsonArray> getUnconfirmedTransactions(String accountRS) {
        Validate.notEmpty(accountRS);

        Map<String, String> map = new HashMap<>();
        map.put("requestType", "getUnconfirmedTransactions");
        map.put("account", accountRS);

        Optional<JsonElement> a = Optional.ofNullable(getJsonObject(getParams(map)).get("unconfirmedTransactions"));
        if (a.isPresent()) {
            return Optional.ofNullable(getJsonObject(getParams(map)).get("unconfirmedTransactions").getAsJsonArray());
        }
        return Optional.empty();

    }

    /**
     *
     * @param accountRS
     * @return
     */
    public Optional<JsonObject> getPlayerBalances(String accountRS) {
        Validate.notEmpty(accountRS);

        Map<String, String> map = new HashMap<>();
        map.put("requestType", "getBalance");
        map.put("account", accountRS);
        return Optional.ofNullable(getJsonObject(getParams(map)));
    }

    /**
     *
     */
    private JsonObject getJsonObject(List<NameValuePair> params) {
        Validate.notNull(params);
        Validate.notEmpty(params);

        JsonObject jsonObject = new JsonObject();
        JsonParser JsonParser = new JsonParser();
        HttpEntity JsonRespEntity;
        DefaultHttpClient JsonHttpClient = new DefaultHttpClient();
        HttpPost JsonHttpPost = new HttpPost(apiServerURL);

        try {
            JsonHttpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            return null;
        }

        try {
            JsonRespEntity = JsonHttpClient.execute(JsonHttpPost).getEntity();
            jsonObject = (JsonObject) JsonParser.parse(EntityUtils.toString(JsonRespEntity));
        } catch (ClientProtocolException e) {
            return null;
        } catch (MalformedJsonException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
        return jsonObject;
    }

    /**
     *
     */
    private List<NameValuePair> getParams(Map<String, String> map) {
        Validate.notNull(map);
        Validate.notEmpty(map);

        List<NameValuePair> params = new ArrayList<>();
        for (Map.Entry<String, String> h : map.entrySet()) {
            params.add(new BasicNameValuePair(h.getKey(), h.getValue()));
        }
        return params;
    }

}
