/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package judgetread.burstcoin.utils;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 *
 * @author o0_pa
 */
public class TitleUpdater {
    
    
    /**
     * Force the inventory to update it title text. This forces it on the
     * clients side.
     *
     * @param player
     * @param title
     */
    public static void updateTitle(Player player, String title) {
        Validate.notNull(title);
        Validate.notNull(player);
        
        if (player.isOnline()) {
            try {
                Object handle = player.getClass().getMethod("getHandle").invoke(player);
                Object message = getNmsClass("ChatMessage").getConstructor(String.class, Object[].class)
                        .newInstance(StrUtils.convertText(title), new Object[0]);
                Object container = handle.getClass().getField("activeContainer").get(handle);
                Object windowId = container.getClass().getField("windowId").get(container);
                Object packet = getNmsClass("PacketPlayOutOpenWindow")
                        .getConstructor(Integer.TYPE, String.class, getNmsClass("IChatBaseComponent"), Integer.TYPE)
                        .newInstance(windowId, "minecraft:chest", message,
                                player.getOpenInventory().getTopInventory().getSize());
                Object playerConnection = handle.getClass().getField("playerConnection").get(handle);
                playerConnection.getClass().getMethod("sendPacket", getNmsClass("Packet")).invoke(playerConnection,
                        packet);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static Class<?> getNmsClass(String nmsClassName) throws ClassNotFoundException {
        return Class.forName("net.minecraft.server."
                + Bukkit.getServer().getClass().getPackage().getName().substring(23) + "." + nmsClassName);
    }
}
